package com.servlet;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

import javax.servlet.AsyncContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.tomcat.util.http.fileupload.FileItem;
import org.apache.tomcat.util.http.fileupload.FileUploadException;
import org.apache.tomcat.util.http.fileupload.disk.DiskFileItemFactory;
import org.apache.tomcat.util.http.fileupload.servlet.ServletFileUpload;
import org.apache.tomcat.util.http.fileupload.servlet.ServletRequestContext;

/*
 * File handler servlet
 */
@WebServlet(name = "Handler", urlPatterns = { "/" }, asyncSupported = true)
public class Handler extends HttpServlet {
    private static final long serialVersionUID = 1L;

    public Handler() {
        super();
    }

    /*
     * Post method with check for uploaded file in request. Sends uploaded file to fileProcessing method 
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        // checks if the request actually contains upload file
        if (!ServletFileUpload.isMultipartContent(request)) {
            PrintWriter writer = response.getWriter();
            writer.println("Request does not contain upload data");
            writer.flush();
            return;
        } else {
            try {
                fileProcessing(request);
                request.setAttribute("message", "Upload has been done successfully!");
            } catch (Exception ex) {
                request.setAttribute("message", "There was an error: " + ex.getMessage());
            }

            getServletContext().getRequestDispatcher("/index.jsp").forward(request, response);
        }

    }

    /*
     * Processes file from request. Saves uploaded file and compresses it
     * @param request
     */
    public void fileProcessing(HttpServletRequest request) throws Exception {
        // File to compress size limit
        String fileCompressionSynchronouslySizeLimit = getServletContext()
                .getInitParameter("FileCompressionSynchronouslySizeLimit");
        int defaultCompressionSizeLimit = 10000;
        long maxFileCompressionSynchronouslySize = (fileCompressionSynchronouslySizeLimit != null)
                ? defaultCompressionSizeLimit
                : Integer.parseInt(fileCompressionSynchronouslySizeLimit);

        // configures upload settings
        DiskFileItemFactory factory = new DiskFileItemFactory();
        factory.setRepository(new File(System.getProperty("java.io.tmpdir")));

        ServletFileUpload upload = new ServletFileUpload(factory);

        String uploadPath = "src/main/resources/compressed";
        // creates the directory if it does not exist
        File uploadDir = new File(uploadPath);
        if (!uploadDir.exists()) {
            uploadDir.mkdir();
        }

        // parses the request's content to extract file data
        List<FileItem> formItems = upload.parseRequest(new ServletRequestContext(request));
        Iterator<FileItem> iter = formItems.iterator();

        while (iter.hasNext()) {
            FileItem item = (FileItem) iter.next();
            // processes only fields that are not form fields
            if (!item.isFormField()) {
                String fileName = new File(item.getName()).getName();
                String filePath = uploadPath + File.separator + fileName;
                File storeFile = new File(filePath);

                item.write(storeFile);

                // Time interval to send request timeout error after
                final int timeoutSec = Integer.parseInt(getServletContext().getInitParameter("InactiveInterval"));
                
                //  If file exceeds max file size limit
                if (Files.size(storeFile.toPath()) > maxFileCompressionSynchronouslySize) {

//                    String threadPoolSize = getServletContext().getInitParameter("ThreadPoolSize");
//                    ExecutorService executor = Executors.newFixedThreadPool(threadPoolSize);

                    // Start asynchronous performance
                    final AsyncContext acontext = request.startAsync();
                    final File file = storeFile;
                    acontext.start(new Runnable() {
                        public void run() {
                            // Start timeout if compression is taking more than set timeout time
                            acontext.setTimeout(timeoutSec);
                            Compress.zipFile(file);
                            acontext.complete();
                        }
                    });
                } else {
                    HttpSession session = request.getSession();
                    // Start timeout if compression is taking more than set timeout time
                    session.setMaxInactiveInterval(timeoutSec);
                    Compress.zipFile(storeFile);
                }
//                  Remove iterator for safe file deletion
                iter.remove();

//                   Delete saved source file 
                storeFile.delete();

            }
        }
    }
}
