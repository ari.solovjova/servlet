package com.servlet;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/*
 * Helper class for file compression
 */
public class Compress  {

    /*
     * Compresses in parameters given file
     * @param filePath file to compress
     */
    public static void zipFile(File file) {
        try {
            String zipFileName = file.getAbsolutePath().concat(".zip");

            FileOutputStream fos = new FileOutputStream(zipFileName);
            ZipOutputStream zos = new ZipOutputStream(fos);

            zos.putNextEntry(new ZipEntry(file.getAbsolutePath()));

            byte[] bytes = Files.readAllBytes(Paths.get(file.getAbsolutePath()));
            zos.write(bytes, 0, bytes.length);
            zos.closeEntry();
            zos.close();

        } catch (FileNotFoundException ex) {
            System.err.format("The file %s does not exist", file);
        } catch (IOException ex) {
            System.err.println("I/O error: " + ex);
        }
    }
}