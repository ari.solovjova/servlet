<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<title>File upload</title>
<meta charset="UTF-8">
</head>
<body>
    <h1>
        <%
        String msg = (String) request.getAttribute("message");
        if (msg != null) {
            out.println(msg);
        }
        %>
    </h1>
    <p>Click on the "Choose File" button to upload a file:</p>
    <form action="Handler" method="post" enctype="multipart/form-data">
        <input type="file" id="myFile" name="filename" /> 
        <br /> 
        <input type="submit" value="Upload" />
    </form>
</body>
</html>